<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

    <title>amarproshno.com</title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body class="home">
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" style="background-color: black" >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="navbar-header">
						<!-- Button for smallest screens -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php">AmarProshno</a>

					</div>
					<div class="navbar-collapse collapse">
						<form class="navbar-form navbar-left" role="search">
							<div class="form-group">
								<input style="min-width: 275px;" type="text" class="form-control" placeholder="Search">
							</div>
							<button class="btn btn-default btn-sm" type="submit">Search</button>

						</form>
						<ul class="nav navbar-nav pull-right">
							<li class="active"><a href="index.php">Home</a></li>
							<li><a href="about.php">About</a></li>


							<li role="presentation" class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									Category <span class="caret"></span>
								</a>
								<ul class="dropdown-menu" style="background-color: black">
									<li><a class="active" href="category.php">Wordpress</a></li>
									<li><a href="category.php">Joomla</a></li>
								</ul>
							</li>

							<li><a href="contact.php">Contact</a></li>
							<li><a class="btn btn-default" href="signin.php">Login / Sign Up</a></li>
							<li><a class="btn btn-default" href="signin.php">Logout</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>
	</div> 
	<!-- /.navbar -->

	<!-- Header -->

	<header id="head">
		<div class="container">
			<div class="row height">
				<form>
					<div class="col-md-12">
                        <div class="col-md-9 form-group">
                            <h2>Ask Your Question...</h2>
                            <textarea class="form-control" id="question" name="question" placeholder="Enter Your Question...." rows="2" required></textarea><br>
                            <button type="button" class="btn btn-default">Continue</button>&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-action">Join Now</button>
                        </div>
                        <div class="col-md-3 select">
                            <select class='mdb-select form-control' style="height: 52px">
                                <option value='total' selected>Select A Category</option>
                                <option value='created'>Technology</option>
                                <option value='read'>Job</option>
                                <option value='replied'>Business</option>
                            </select>
                        </div>
                    </div>
				</form>

			</div>
		</div>
	</header>
<br><br>

	<div class="container">
		<div class="row">

            <div class="col-md-9">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="index.php">Recent</a></li>
                    <li role="presentation"><a href="#">Popular</a></li>
                    <li role="presentation"><a href="#">Unanswered</a></li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">
                            <a href="#">
                                <span class="glyphicon  glyphicon-tags"></span> In Business
                            </a>
                            | <span class="glyphicon  glyphicon-time"></span> 2 Minute ago | <span class="glyphicon
                        glyphicon-comment"></span> 9 Answer
                        </h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-3">
                            <label class="btn btn-secondery">
                                <img src="assets/images/1.jpg">
                                <a href="#"><p>username</p></a>
                            </label>
                        </div>
                        <div class="col-md-9">
                            <h6 style="font-size: 22px;">
                                What is wordpress?
                                <nav aria-label="...">
                                    <ul class="pager">
                                        <li class="next"><a href="details.php">Answer <span class="glyphicon  glyphicon-hand-right"
                                                                                            aria-hidden="true"></span></a></li>
                                    </ul>
                                </nav>
                            </h6>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">
                            <a href="#">
                                <span class="glyphicon  glyphicon-tags"></span> In Business
                            </a>
                            | <span class="glyphicon  glyphicon-time"></span>  2 Minute ago | <span class="glyphicon  glyphicon-comment"></span> 9 Answer
                        </h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-3">
                            <label class="btn btn-secondery">
                                <img src="assets/images/1.jpg">
                                <a href="#"><p>username</p></a>
                            </label>
                        </div>
                        <div class="col-md-9">
                            <h6 style="font-size: 22px;">
                                What is wordpress?
                                <nav aria-label="...">
                                    <ul class="pager">
                                        <li class="next"><a href="details.php">Answer <span class="glyphicon  glyphicon-hand-right"
                                                                                            aria-hidden="true"></span></a></li>
                                    </ul>
                                </nav>
                            </h6>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">
                            <a href="#">
                                <span class="glyphicon  glyphicon-tags"></span>  In Business
                            </a>
                            | <span class="glyphicon  glyphicon-time"></span> 2 Minute ago | <span class="glyphicon  glyphicon-comment"></span> 9 Answer
                        </h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-3">
                            <label class="btn btn-secondery">
                                <img src="assets/images/1.jpg">
                                <a href="#"><p>username</p></a>
                            </label>
                        </div>
                        <div class="col-md-9">
                            <h6 style="font-size: 22px;">
                                What is wordpress?
                                <nav aria-label="...">
                                    <ul class="pager">
                                        <li class="next"><a href="details.php">Answer <span class="glyphicon  glyphicon-hand-right"
                                                                                            aria-hidden="true"></span></a></li>
                                    </ul>
                                </nav>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
<br><br>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">Site Stats</h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-10">
                            <a href="#"><span class="glyphicon  glyphicon-question-sign"></span> Total Questions <span class="badge">42</span></a><br><br>
                            <a href="#"><span class="glyphicon  glyphicon-ok-circle"></span> Total Answers <span class="badge">35</span></a>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">Login</h2>
                    </div>
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="email" name="password" placeholder="Enter Your Password" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-default btn-xs" name="submit">Login</button>
                                <button type="submit" class="btn btn-action btn-xs" name="submit">Join</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
	</div>



	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p>+8801911543307<br>
								<a href="mailto:#">turtles_group@gmail.com</a><br>
								<br>
								Jatrabari, Dhaka, 1204
							</p>	
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Follow Us</h3>
						<div class="widget-body">
							<p class="follow-me-icons">
								<a href=""><i class="fa fa-twitter fa-2"></i></a>
								<a href=""><i class="fa fa-dribbble fa-2"></i></a>
								<a href=""><i class="fa fa-github fa-2"></i></a>
								<a href=""><i class="fa fa-facebook fa-2"></i></a>
							</p>	
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">Text widget</h3>
						<div class="widget-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, dolores, quibusdam architecto voluptatem amet fugiat nesciunt placeat provident cumque accusamus itaque voluptate modi quidem dolore optio velit hic iusto vero praesentium repellat commodi ad id expedita cupiditate repellendus possimus unde?</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<a href="about.php">About</a> |
								<a href="contact.php">Contact</a> |
								<b><a href="signup.php">Sign up</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; <?php date('Y'); ?>, The Turtles Group. <a href="#" rel="designer">Turtles</a>
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
</body>
</html>