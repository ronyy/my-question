<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

    <title>amarproshno.com</title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top headroom" style="background-color: black" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <!-- Button for smallest screens -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">AmarProshno</a>

                    </div>
                    <div class="navbar-collapse collapse">
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input style="min-width: 275px;" type="text" class="form-control" placeholder="Search">
                            </div>
                            <button class="btn btn-default btn-sm" type="submit">Search</button>

                        </form>
                        <ul class="nav navbar-nav pull-right">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="about.php">About</a></li>


                            <li role="presentation" class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    Category <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" style="background-color: black">
                                    <li><a href="category.php">Wordpress</a></li>
                                    <li><a href="category.php">Joomla</a></li>
                                </ul>
                            </li>

                            <li><a href="contact.php">Contact</a></li>
                            <li><a class="btn btn-default" href="signin.php">Login / Sign Up</a></li>
                            <li><a class="btn btn-default" href="signin.php">Logout</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="index.php">Home</a></li>
			<li class="active">About</li>
		</ol>

		<div class="row">

            <div id="portfolio" class="container-fluid text-center">
                <h2 class="panel-heading">The Turtles Group</h2><br>
                <h4>Our Team Members Are....</h4><br>
                <div class="row">
                    <div class="col-md-3">
                        <img src="assets/images/sadek.jpg" class="img-circle" alt="Cinque Terre" width="270"
                             height="220"> <br><br>
                        <p><strong>Mohiuddin Sadek</strong></p>
                        <p>Best kamla of the turtles group.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/suhag.jpg" class="img-circle" alt="Cinque Terre" width="270"
                             height="220"> <br><br>
                        <p><strong>MH Suhag</strong></p>
                        <p>Best fakibujj of the turtles group.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/rony.jpg" class="img-circle" alt="Cinque Terre" width="270"
                             height="220"> <br><br>
                        <p><strong>Abu Hanifa Rony</strong></p>
                        <p>Medium fakibujj of the turtles group.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/rayhan.jpg" class="img-circle" alt="Cinque Terre" width="270"
                             height="220"> <br><br>
                        <p><strong>Rayhan Islam</strong></p>
                        <p>Oshohay Member of the turtles group.</p>
                    </div>
                </div>
            </div>

		</div>
	</div>	<!-- /container -->


    <footer id="footer" class="top-space">

        <div class="footer1">
            <div class="container">
                <div class="row">

                    <div class="col-md-3 widget">
                        <h3 class="widget-title">Contact</h3>
                        <div class="widget-body">
                            <p>+8801911543307<br>
                                <a href="mailto:#">turtles_group@gmail.com</a><br>
                                <br>
                                Jatrabari, Dhaka, 1204
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 widget">
                        <h3 class="widget-title">Follow Us</h3>
                        <div class="widget-body">
                            <p class="follow-me-icons">
                                <a href=""><i class="fa fa-twitter fa-2"></i></a>
                                <a href=""><i class="fa fa-dribbble fa-2"></i></a>
                                <a href=""><i class="fa fa-github fa-2"></i></a>
                                <a href=""><i class="fa fa-facebook fa-2"></i></a>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 widget">
                        <h3 class="widget-title">Text widget</h3>
                        <div class="widget-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, dolores, quibusdam architecto voluptatem amet fugiat nesciunt placeat provident cumque accusamus itaque voluptate modi quidem dolore optio velit hic iusto vero praesentium repellat commodi ad id expedita cupiditate repellendus possimus unde?</p>
                        </div>
                    </div>

                </div> <!-- /row of widgets -->
            </div>
        </div>

        <div class="footer2">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 widget">
                        <div class="widget-body">
                            <p class="simplenav">
                                <a href="#">Home</a> |
                                <a href="about.php">About</a> |
                                <a href="contact.php">Contact</a> |
                                <b><a href="signup.php">Sign up</a></b>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 widget">
                        <div class="widget-body">
                            <p class="text-right">
                                Copyright &copy; <?php date('Y'); ?>, The Turtles Group. <a href="#" rel="designer">Turtles</a>
                            </p>
                        </div>
                    </div>

                </div> <!-- /row of widgets -->
            </div>
        </div>

    </footer>





    <!-- JavaScript libs are placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/headroom.min.js"></script>
    <script src="assets/js/jQuery.headroom.min.js"></script>
    <script src="assets/js/template.js"></script>
</body>
</html>